set relativenumber
set nu
set hidden
set incsearch
set scrolloff=6
set termguicolors
set shiftwidth=4
set tabstop=2
set softtabstop=2
syntax enable
set clipboard+=unnamedplus
filetype plugin indent on
set signcolumn=yes
set expandtab
set cursorline
set nowrap

set completeopt=menuone,noinsert,noselect
set shortmess+=c

call plug#begin('~/.vim/plugged')
Plug 'phanviet/vim-monokai-pro'
Plug 'neovim/nvim-lspconfig'
Plug 'neovim/nvim-lsp'
Plug 'preservim/nerdcommenter'
Plug 'preservim/nerdtree'
Plug 'OmniSharp/omnisharp-vim'
Plug 'junegunn/fzf', {'do': {-> fzf#install()}}
Plug 'junegunn/fzf.vim'
Plug 'airblade/vim-rooter'
Plug 'nvim-treesitter/nvim-treesitter'
Plug 'nvim-lualine/lualine.nvim'
Plug 'tpope/vim-fugitive'

" auto-complete
Plug 'hrsh7th/cmp-nvim-lsp'
Plug 'hrsh7th/cmp-buffer'
Plug 'hrsh7th/cmp-path'
Plug 'hrsh7th/cmp-cmdline'
Plug 'hrsh7th/nvim-cmp'
Plug 'hrsh7th/cmp-vsnip'
Plug 'hrsh7th/vim-vsnip'
call plug#end()

colorscheme monokai_pro
highlight Normal guibg=none
highlight EndOfBuffer guibg=none

source ~/.config/nvim/vim/keybinds.vim

luafile ~/.config/nvim/lua/completion.lua
luafile ~/.config/nvim/lua/treesitter.lua
luafile ~/.config/nvim/lua/line.lua
