let mapleader = " "

" lsp
nnoremap <silent> gd <cmd>lua vim.lsp.buf.definition()<CR>
nnoremap <silent> <leader>h <cmd>lua vim.lsp.buf.hover()<CR>
nnoremap <silent> gD <cmd>lua vim.lsp.buf.implementation()<CR>
nnoremap <silent> <leader>r <cmd>lua vim.lsp.buf.references()<CR>
nnoremap <silent> <leader>d <cmd>lua vim.lsp.buf.declaration()<CR>
nnoremap <silent> <leader>a <cmd>lua vim.lsp.buf.code_action()<CR>
nnoremap <silent> <leader>[ <cmd>lua vim.lsp.diagnostic.goto_prev()<CR>
nnoremap <silent> <leader>] <cmd>lua vim.lsp.diagnostic.goto_next()<CR>

" nerd commenter
nnoremap <silent> <leader>cc <cmd>NERDCommenterComment<CR>
nnoremap <silent> <leader>cu <cmd>NERDCommenterUncomment<CR>

" nerd tree
" nnoremap <silent> <leader>t <cmd>NERDTreeToggle<CR>
nnoremap <silent> <leader>t <cmd>terminal<CR>

" fzf
nnoremap <silent> <leader>f <cmd>:Files<CR>
nnoremap <silent> <leader>gf <cmd>:GFiles<CR>
nnoremap <silent> <leader>gl <cmd>:GFiles?<CR>
nnoremap <silent> <leader>rg <cmd>:Rg<CR>

" vim fugitive
nnoremap <silent> <leader>s <cmd>:G<CR>
